﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MessageBox.Show("你好窗体");
            
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            BackColor = Color.Blue;

            var res=  MessageBox.Show("消息得主要类容","消息标题" ,MessageBoxButtons.YesNo,MessageBoxIcon.Information);
            if (res == DialogResult.OK)
            {
                MessageBox.Show("你点击同意了，就嫁给我");
            }
            else
            {
                MessageBox.Show("你点击了不同意，我就娶了你");
            }
        }

    }
}
